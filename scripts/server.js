var Server = {};
var ServerEvents = new EventTarget();

Server.init = function() {
	Server.io = io.connect("https://spotim-demo-chat-server.herokuapp.com");
	Server.io.on("connect", Server.onConnect);
	Server.io.on("connect_error", Server.onConnectError);
	Server.io.on("disconnect", Server.onDisconnect);
	Server.io.on("spotim/chat", Server.onChatMessage);
	Server.io.connect();
}

Server.send = function(obj) {
	Server.io.emit("spotim/chat", obj);
}

Server.onConnect = function() {
	console.log("connected");
}

Server.onConnectError = function() {
	console.log("connection error");
}

Server.onDisconnect = function() {
	console.log("disconnected");
}

Server.onChatMessage = function(data) {
	ServerEvents.fireEvent("onChatMessage", data)
}