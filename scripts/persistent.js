var PersistentData = {};

PersistentData.getHistory = function() {
	return JSON.parse(window.localStorage.getItem("history"));
}

PersistentData.setHistory = function(messages) {
	return window.localStorage.setItem("history", JSON.stringify(messages));
}

PersistentData.getAvatar = function() {
	return window.localStorage.getItem("avatar");
}

PersistentData.setAvatar = function(id) {
	window.localStorage.setItem("avatar", id);
}

PersistentData.getUniqueId = function() {
	return window.localStorage.getItem("unique_id");
}

PersistentData.setUniqueId = function(id) {
	window.localStorage.setItem("unique_id", id);
}

PersistentData.getUsername = function() {
	return window.localStorage.getItem("username");
}

PersistentData.setUsername = function(username) {
	window.localStorage.setItem("username", username);
}