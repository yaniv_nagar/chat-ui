var App = {};

App.init = function() {
	Server.init();
	Chat.init();
}

window.onload = App.init;