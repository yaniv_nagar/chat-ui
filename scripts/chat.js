var Chat = {};

Chat.avatars = [
	"https://spotim-demo-chat-server.herokuapp.com/avatars/001-snorlax.png",
	"https://spotim-demo-chat-server.herokuapp.com/avatars/002-psyduck.png",
	"https://spotim-demo-chat-server.herokuapp.com/avatars/003-pikachu.png",
	"https://spotim-demo-chat-server.herokuapp.com/avatars/004-jigglypuff.png",
	"https://spotim-demo-chat-server.herokuapp.com/avatars/005-bullbasaur.png"
];

Chat.init = function() {
	ServerEvents.addEventListener("onChatMessage", Chat.onChatMessage);
	Chat.history = PersistentData.getHistory() || [];
	Chat.avatar_id = PersistentData.getAvatar() || Chat.generateAvatar();
	Chat.unique_id = PersistentData.getUniqueId() || Chat.generateUniqueId();
	Chat.username = PersistentData.getUsername() || "";
	$("#username").val(Chat.username);
	Chat.populateHistory();
}

Chat.populateHistory = function() {
	for (var i=0;i<Chat.history.length;i++) {
		Chat.onChatMessage(Chat.history[i], true);
	}
}

Chat.send = function() {
	$("#chat_text,#username").removeClass("input_error");
	if ($.trim($("#chat_text").val()) != "") {
		if ($.trim($("#username").val()) != "") {
			Chat.username = $("#username").val();
			PersistentData.setUsername(Chat.username);

			Server.send({
				avatar:Chat.avatar_id,
				username:Chat.username,
				text:$.trim($("#chat_text").val()),
				unique_id:Chat.unique_id
			});
			$("#chat_text").val("");
		}
		else {
			$("#username").addClass("input_error");
		}
	}
	else {
		$("#chat_text").addClass("input_error");
	}
}

Chat.onChatMessage = function(data, from_history) {
	if (!from_history) {
		Chat.history.push(data);
		PersistentData.setHistory(Chat.history);
	}

	var message = $($("#message_row_template").html());
	message.css("background-image", "url('" + Chat.avatars[data.avatar] + "')");
	message.find(".username").html(data.username);
	message.find(".message").html(data.text);
	if (data.unique_id == Chat.unique_id)
		message.addClass("blue lighten-3");
	$(".messages_area").append(message);
}

Chat.generateAvatar = function() {
	var avatar_id = Math.floor(Math.random() * (Chat.avatars.length));
	PersistentData.setAvatar(avatar_id);
	return avatar_id;
}

Chat.generateUniqueId = function() {
	var unique_id = Math.floor(Math.random() * 1000000000000);
	PersistentData.setUniqueId(unique_id);
	return unique_id;
}